<?php

class Accounts extends CModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getInfo($id)
    {
        $result = $this->db->select('
            SELECT id, email, firstname, lastname, password
            FROM accounts
            WHERE id = :id',
            array(':id' => (int)$id)
        );
        if(count($result) > 0){
            return $result[0];
        }else{
            return array('email'=>'', 'password'=>'');    
        }        
    }
    
    public function save($firstname,$lastname,$email, $password)
    {
        if($password != ""){
        $result = $this->db->update(
            'accounts',
            array(
                'email' => $email,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'password' => ((CConfig::get('password.encryption')) ? CHash::create(CConfig::get('password.encryptAlgorithm'), $password, CConfig::get('password.hashKey')) : $password)
            ),
            'id = '.(int)CAuth::getLoggedId()
        );
	}else{
$result = $this->db->update(
            'accounts',
            array(
                'email' => $email,
                'firstname' => $firstname,
                'lastname' => $lastname
            ),
            'id = '.(int)CAuth::getLoggedId()
        );
	}
        return $result;
    }   

    public function signup($firstname,$lastname,$email, $password)
    {
        $result = $this->db->insert(
            'accounts',
            array(
                'email' => $email,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'password' => ((CConfig::get('password.encryption')) ? CHash::create(CConfig::get('password.encryptAlgorithm'), $password, CConfig::get('password.hashKey')) : $password)
            )
        );
        if($result){
		$model = new Login();				
		return $model->login($email, $password);
	}
        return $result;
    }    
}
