<?php

class Login extends CModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function login($email, $password)
    {
        $result = $this->db->select('
            SELECT id, role,firstname, lastname
            FROM '.CConfig::get('db.prefix').'accounts
            WHERE email = :email AND password = :password',
            array(
                ':email' => $email,
                ':password' => ((CConfig::get('password.encryption')) ? CHash::create(CConfig::get('password.encryptAlgorithm'), $password, CConfig::get('password.hashKey')) : $password)
            )
        );

        if(!empty($result)){
            $session = A::app()->getSession();
            $session->set('role', $result[0]['role']);
            $session->set('loggedIn', true);
            $session->set('loggedId', $result[0]['id']);
            $session->set('loggedName', $result[0]['firstname'].' '.$result[0]['lastname']);

            return true;
        }else{
            return false;        
        }        
    }    
}
