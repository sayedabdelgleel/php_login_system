<?php
    $this->_activeMenu = 'signup/index';
?>

<div style="width:400px; margin:100px auto;">


<?php echo $actionMessage; ?>

<fieldset>
    <legend>Sign Up</legend>
    
    <?php
        // draw login form
        echo CWidget::create('CFormView', array(
            'action'=>'signup/run',
            'method'=>'post',
            'htmlOptions'=>array(
                'name'=>'frmLogin'
            ),
            'fields'=>array(
                'act'     =>array('type'=>'hidden', 'value'=>'send'),

                'firstname'=>array('type'=>'textbox', 'value'=>$firstname, 'title'=>'First Name', 'mandatoryStar'=>true, 'htmlOptions'=>array('autocomplete'=>'off')),
                'lastname'=>array('type'=>'textbox', 'value'=>$lastname, 'title'=>'Last Name', 'mandatoryStar'=>true, 'htmlOptions'=>array('autocomplete'=>'off')),

                'email'=>array('type'=>'textbox', 'value'=>$email, 'title'=>'Email', 'mandatoryStar'=>true, 'htmlOptions'=>array( 'autocomplete'=>'off')),
                'password'=>array('type'=>'password', 'value'=>$password, 'title'=>'Password', 'mandatoryStar'=>true, 'htmlOptions'=>array())
            ),
            'buttons'=>array(
                'submit'=>array('type'=>'submit', 'value'=>'Sign Up'),
            ),
            'events'=>array(
                'focus'=>array('field'=>$errorField)
            ),
            'return'=>true,
        ));    
    ?>
    
    </fieldset>
</div>
