<?php
    $this->_activeMenu = 'login/index';
?>

<div style="width:400px; margin:100px auto;">


<?php echo $actionMessage; ?>

<fieldset>
    <legend>Login</legend>
    
    <?php
        // draw login form
        echo CWidget::create('CFormView', array(
            'action'=>'login/run',
            'method'=>'post',
            'htmlOptions'=>array(
                'name'=>'frmLogin'
            ),
            'fields'=>array(
                'act'     =>array('type'=>'hidden', 'value'=>'send'),
                'email'=>array('type'=>'textbox', 'value'=>$email, 'title'=>'Email', 'mandatoryStar'=>false, 'htmlOptions'=>array('maxlength'=>'100', 'autocomplete'=>'off')),
                'password'=>array('type'=>'password', 'value'=>$password, 'title'=>'Password', 'mandatoryStar'=>false, 'htmlOptions'=>array('maxLength'=>'200')),
            ),
            'buttons'=>array(
                'submit'=>array('type'=>'submit', 'value'=>'Login'),
            ),
            'events'=>array(
                'focus'=>array('field'=>$errorField)
            ),
            'return'=>true,
        ));    
    ?>
    
    </fieldset>
</div>
