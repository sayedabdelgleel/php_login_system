<?php
    $this->_activeMenu = 'account/edit';
?>

<?php echo $actionMessage; ?>

<fieldset>
    <legend>Edit My Account</legend>

    <?php
        echo CWidget::create('CFormView', array(
            'action'=>'account/edit',
            'method'=>'post',
            'htmlOptions'=>array(
                'name'=>'frmAccount',
                'enctype'=>'multipart/form-data'
            ),
            'fields'=>array(
                'act'     =>array('type'=>'hidden', 'value'=>'send'),
                                'firstname'=>array('type'=>'textbox', 'value'=>$firstname, 'title'=>'First Name', 'mandatoryStar'=>true, 'htmlOptions'=>array('autocomplete'=>'off')),
                'lastname'=>array('type'=>'textbox', 'value'=>$lastname, 'title'=>'Last Name', 'mandatoryStar'=>true, 'htmlOptions'=>array('autocomplete'=>'off')),

                'email'=>array('type'=>'textbox', 'value'=>$email, 'title'=>'Email', 'mandatoryStar'=>true, 'htmlOptions'=>array( 'autocomplete'=>'off')),
                'password'=>array('type'=>'password', 'value'=>$password, 'title'=>'Password', 'mandatoryStar'=>true, 'htmlOptions'=>array('maxLength'=>'20', 'placeholder'=>'&#9679;&#9679;&#9679;&#9679;&#9679;')),
            ),
            'buttons'=>array(
                'submit'=>array('type'=>'submit', 'value'=>'Update')
            ),
            'events'=>array(
                'focus'=>array('field'=>$errorField)
            ),
            'return'=>true,
        ));
    ?>    

</fieldset>

