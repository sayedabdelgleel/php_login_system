<?php

class SignupController extends CController
{
    
	public function __construct()
	{
        	parent::__construct();
		
        	$this->view->errorField = '';
		$this->view->actionMessage = '';
		$this->view->username = '';
		$this->view->password = '';
		$this->view->email = '';
    	}

	public function indexAction()
	{
		CAuth::handleLoggedIn('page/dashboard');

		$this->view->render('login/index');	
	}

	public function logoutAction()
	{
        A::app()->getSession()->endSession();
        $this->redirect('login/index');
	}
	
	public function runAction()
	{
		$cRequest = A::app()->getRequest();
		$this->view->firstname = $cRequest->getPost('firstname');
		$this->view->lastname = $cRequest->getPost('lastname');
		$this->view->email = $cRequest->getPost('email');
        	$this->view->password = $cRequest->getPost('password');

		
		$msg = '';
		$errorType = '';        		
        
		if($cRequest->getPost('act') == 'send'){

            // perform login form validation
            $result = CWidget::create('CFormValidation', array(
                'fields'=>array(
                    'firstname'=>array('title'=>'First Name', 'validation'=>array('required'=>true, 'type'=>'alphaNumeric')),
                    'lastname'=>array('title'=>'Last Name', 'validation'=>array('required'=>true, 'type'=>'alpha')),
                    'email'=>array('title'=>'Email', 'validation'=>array('required'=>true, 'type'=>'email')),
		    'password'=>array('title'=>'Password', 'validation'=>array('required'=>true, 'type'=>'password'))
                ),            
            ));
            
            if($result['error']){
				$msg = $result['errorMessage'];
				$this->view->errorField = $result['errorField'];
				$errorType = 'validation';                
            }else{
				$model = new Accounts();				
				if($model->signup($this->view->firstname,$this->view->lastname, $this->view->email, $this->view->password)){
					$this->redirect('page/dashboard');	
				}else{
					$msg = 'Wrong username or password! Please re-enter.';
					$this->view->errorField = 'username';
					$errorType = 'error';
				}                
            }
        
			if(!empty($msg)){				
				$this->view->firstname = $cRequest->getPost('firstname', 'string');
				$this->view->lastname = $cRequest->getPost('lastname', 'string');
				$this->view->email = $cRequest->getPost('email', 'string');
				$this->view->password = $cRequest->getPost('password', 'string');				
				$this->view->actionMessage = CWidget::create('CMessage', array($errorType, $msg));
			}			
        }
		$this->view->render('login/index');	
	}
    
}
