<?php

class IndexController extends CController
{
    
	public function __construct()
	{
        parent::__construct();

		$this->view->setMetaTags('title', 'Sample application - Simple Login System : Index');
		$this->view->setMetaTags('keywords', ' simple login system');
		$this->view->setMetaTags('description', 'This is a simple login system, consists from the few pages and protected area.');
    }
	
	public function indexAction()
	{
        $this->view->header = 'Simple Login System';
        $this->view->text = '
			Welcome to FTC Telecom.
			<br><br>
			Click links from the top menu to see the site in work.
		';
        $this->view->render('index/index');		
    }
	
}
