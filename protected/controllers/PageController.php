<?php

class PageController extends CController
{
	public $id;
	
	public function __construct()
	{
        parent::__construct();
		
		$this->view->setMetaTags('title', 'Sample application - Simple Login System : Index');
		$this->view->setMetaTags('keywords', 'apphp framework, simple login system, apphp');
		$this->view->setMetaTags('description', 'This is a simple login system, consists from the few pages and protected area.');
    }
	
	public function indexAction()
	{
		$this->redirect('index/index');
    }

	public function errorAction()
	{
        $this->view->header = 'Error 404';
        $this->view->text = CDebug::getMessage('errors', 'action').'<br>Please check carefully the URL you\'ve typed.';		
        $this->view->render('error/index');        
    }
    
	public function publicAction($id = 0)
	{
		$id = (CValidator::isDigit($id) && CValidator::validateMaxlength($id, 1)) ? $id : 0;		
		$this->view->id = $id;
        $this->view->header = 'Page'.$id;
		if($id === '1'){
			$this->view->text = '
				Welcome to FTC Telecom Page #1.
			';
		}else{
			$this->view->text = '
				Welcome to FTC Telecom Page #2.
			';
		}
		
        $this->view->render('page/index');		
    }
	
	public function dashboardAction()
	{
		CAuth::handleLogin();	
		$this->view->username = CAuth::getLoggedName();
		$this->view->render('page/dashboard');		
	}	

}
