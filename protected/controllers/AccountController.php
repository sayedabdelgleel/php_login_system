<?php

class AccountController extends CController
{

	public function __construct()
	{
        parent::__construct();

        // block access to this controller for not-logged users
		CAuth::handleLogin();		
        
		$this->view->setMetaTags('title', 'Sample application - Simple Login System : My Account');
		$this->view->setMetaTags('keywords', 'apphp framework, simple login system, apphp');
		$this->view->setMetaTags('description', 'This is a simple login system, consists from the few pages and protected area.');
    }

   	public function editAction()
	{
        $cRequest = A::app()->getRequest();
		$this->view->errorField = '';
		$this->view->actionMessage = '';
		$this->view->firstname = $cRequest->getPost('firstname');
		$this->view->lastname = $cRequest->getPost('lastname');
		$this->view->email = $cRequest->getPost('email');
        	$this->view->password = $cRequest->getPost('password');

		$msg = '';
		$errorType = '';

        $model = new Accounts();
        $info = $model->getInfo(CAuth::getLoggedId());
        
		if($cRequest->getPost('act') == 'send'){


            // perform account edit form validation
            if($this->view->password != "" ){
            $result = CWidget::create('CFormValidation', array(
                'fields'=>array(
                    'firstname'=>array('title'=>'First Name', 'validation'=>array('required'=>true, 'type'=>'alphaNumeric')),
                    'lastname'=>array('title'=>'Last Name', 'validation'=>array('required'=>true, 'type'=>'alpha')),
                    'password'=>array('title'=>'Password', 'validation'=>array('required'=>true, 'type'=>'password')),
                    'email'=>array('title'=>'Email', 'validation'=>array('required'=>true, 'type'=>'email'))
                ),            
            ));
	    }else{
	   $result = CWidget::create('CFormValidation', array(
                'fields'=>array(
                    'firstname'=>array('title'=>'First Name', 'validation'=>array('required'=>true, 'type'=>'alphaNumeric')),
                    'lastname'=>array('title'=>'Last Name', 'validation'=>array('required'=>true, 'type'=>'alpha')),
                    'email'=>array('title'=>'Email', 'validation'=>array('required'=>true, 'type'=>'email'))
                ),            
            ));
	    }

            if($result['error']){
				$msg = $result['errorMessage'];
				$this->view->errorField = $result['errorField'];
				$errorType = 'validation';                
            }else{
                                   
                    if($model->save($this->view->firstname, $this->view->lastname, $this->view->email, $this->view->password)){
                        $msg = 'Account has been successfully saved!';
                        $errorType = 'success';
                    }else{
                        $msg = 'An error occurred while saving Please re-enter.';
                        $this->view->errorField = 'password';
                        $errorType = 'error';
                    }
                
            }

			if(!empty($msg)){				
				$this->view->firstname = $cRequest->getPost('firstname', 'string');
				$this->view->lastname = $cRequest->getPost('lastname', 'string');
				$this->view->email = $cRequest->getPost('email', 'string');
		
				
				$this->view->actionMessage = CWidget::create('CMessage', array($errorType, $msg, array('button'=>true)));				
			}else{
                $this->view->email = $info['email'];
		$this->view->firstname = $info['firstname'];
		$this->view->lastname = $info['lastname'];
		
            }
        }else{
                $this->view->email = $info['email'];
		$this->view->firstname = $info['firstname'];
		$this->view->lastname = $info['lastname'];
		
        }
        
       
        $this->view->render('account/edit');		
    }

}
