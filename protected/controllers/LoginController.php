<?php

class LoginController extends CController
{
    
	public function __construct()
	{
        parent::__construct();
		
        $this->view->errorField = '';
		$this->view->actionMessage = '';
		$this->view->email = '';
		$this->view->password = '';
    }

	public function indexAction()
	{
		CAuth::handleLoggedIn('page/dashboard');

		$this->view->render('login/index');	
	}

	public function logoutAction()
	{
        A::app()->getSession()->endSession();
        $this->redirect('login/index');
	}
	
	public function runAction()
	{
		$cRequest = A::app()->getRequest();
		$this->view->email = $cRequest->getPost('email');
		$this->view->password = $cRequest->getPost('password');
		$msg = '';
		$errorType = '';        		
        
		if($cRequest->getPost('act') == 'send'){

            // perform login form validation
            $result = CWidget::create('CFormValidation', array(
                'fields'=>array(
                    'email'=>array('title'=>'Email', 'validation'=>array('required'=>true, 'type'=>'email')),
                    'password'=>array('title'=>'Password', 'validation'=>array('required'=>true, 'type'=>'password')),
                ),            
            ));
            
            if($result['error']){
				$msg = $result['errorMessage'];
				$this->view->errorField = $result['errorField'];
				$errorType = 'validation';                
            }else{
				$model = new Login();				
				if($model->login($this->view->email, $this->view->password)){
                                	        
					$this->redirect('page/dashboard');	
				}else{
					$msg = 'Wrong Email or password! Please re-enter.';
					$this->view->errorField = 'email';
					$errorType = 'error';
				}                
            }
        
			if(!empty($msg)){				
				$this->view->email = $cRequest->getPost('email', 'string');
				$this->view->password = $cRequest->getPost('password', 'string');				
				$this->view->actionMessage = CWidget::create('CMessage', array($errorType, $msg));
			}			
        }
		$this->view->render('login/index');	
	}
    
}
