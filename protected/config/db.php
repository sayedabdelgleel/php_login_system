<?php            
return array(
    // database settings
    'db' => array(
        'type' => 'mysql',
        'host' => 'localhost:3306',
        'database' => 'test',
        'username' => 'root',
        'password' => 'root',
        'prefix' => '',
    )               
);
